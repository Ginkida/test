<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Order;

class IndexController extends Controller
{
    public function index()
    {
        return view('form.index');
    }

    public function save(Request $request)
    {
        $order = Order::create($request->all());

        return response()->json($order);
    }

    public function get()
    {
        $orders = Order::get();

        return response()->json($orders);
    }

    public function edit(Request $request)
    {
        $order = Order::whereId($request->input('id'))->firstOrFail();

        $order->update($request->except('id'));

        return response()->json($order);
    }
}
