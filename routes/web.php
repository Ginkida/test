<?php

Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
Route::post('/save', ['as' => 'save', 'uses' => 'IndexController@save']);
Route::get('/get', ['as' => 'get', 'uses' => 'IndexController@get']);
Route::post('/edit', ['as' => 'edit', 'uses' => 'IndexController@edit']);
