@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-2 order-md-1">
            <h4 class="mb-3">Products form</h4>
            <form-component></form-component>
        </div>
    </div>
@endsection